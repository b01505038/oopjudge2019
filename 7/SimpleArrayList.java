//package hw7;

import java.util.HashSet;

public class SimpleArrayList {
	
	static final int DEFAULT_ARR_CAPACITY = 32;

	private Integer[] arr;
        int arr_size;
        int arr_capacity;

	public SimpleArrayList() {
                this(0);
	}

	public SimpleArrayList(int initialSize) {
                this.arr_capacity = DEFAULT_ARR_CAPACITY;
                this.arr_size = initialSize;
                while (this.arr_size > this.arr_capacity)
                        this.arr_capacity *= 2;
		this.arr = new Integer[arr_capacity];

                for (int i = 0; i < arr_size; ++i)
                        arr[i] = new Integer(0);
	}


	public void add(int element) {
		++arr_size;
                if (arr_size > arr_capacity)
                        doubleArrCapacity();
                arr[arr_size - 1] = element;
	}

	public Integer get(int index) {
                return ((index >= 0 && index < arr_size)? arr[index] : null);
	}

	public Integer set(int index, Integer element) {
                if (index >= 0 && index < arr_size) {
                        Integer tmp = arr[index];
                        arr[index] = element;
                        return tmp;
                } else {
                        return null;
                }
	}

	public boolean remove(int index) {
                if ((index >= 0 && index < arr_size)
                    || arr[index] != null) {
                        int i;

                        for (i = index; i < arr_size - 1; ++i)
                                arr[i] = arr[i + 1];
                        arr[i + 1] = null;
                        --arr_size;

                        return true;
                } else {
                        return false;
                }
	}

	public void clear() {
                for (int i = 0; i < arr_size; ++i)
                        arr[i] = null;
                arr_size = 0;
	}

	public int size() {
                return arr_size;
	}

	public boolean retainAll(SimpleArrayList l) {
                int l_arr_size = l.arr_size;
                Integer[] l_arr = l.arr;

                HashSet<Integer> hs = new HashSet<Integer>(l_arr_size);

                for (int i = 0; i < l_arr_size; ++i)
                        hs.add(l_arr[i]);

                Integer[] new_arr = new Integer[arr_capacity];
                int new_arr_size = 0;
                for (int i = 0; i < arr_size; ++i) {
                        if (hs.contains(arr[i])) {
                                new_arr[new_arr_size] = arr[i];
                                ++new_arr_size;
                        }
                }

                boolean ret = ((arr_size - new_arr_size) > 0)? true : false;
                arr = new_arr;
                arr_size = new_arr_size;

                return ret;
	}

        private void doubleArrCapacity() {
                arr_capacity *= 2;

                Integer[] new_arr = new Integer[arr_capacity];
                for (int i = 0; i < arr_size; ++i)
                        new_arr[i] = arr[i];
                arr = new_arr;
        }
}
