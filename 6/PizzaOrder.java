

public class PizzaOrder {
	
	public class Main {
		public void main(String[] args){
	                Pizza pizza1 = new Pizza("large", 1, 0, 1);
	                Pizza pizza2 = new Pizza("medium", 2, 2, 5);
	                Pizza pizza3 = new Pizza();
	                PizzaOrder order = new PizzaOrder();
	                System.out.println(order.setNumberPizzas(5));

	                order.setNumberPizzas(2);
	                order.setPizza1(pizza1);
	                order.setPizza2(pizza2);
	                System.out.println(order.calcTotal());

	                order.setNumberPizzas(3);
	                order.setPizza1(pizza1);
	                order.setPizza2(pizza2);
	                order.setPizza3(pizza3);
	                System.out.println(order.calcTotal());
		}
	}

	
	
	
	private int numPizzas;
	private Pizza pizza1;
	private Pizza pizza2;
	private Pizza pizza3;

        public PizzaOrder() {
                this.pizza1 = new Pizza();
                this.pizza2 = new Pizza();
                this.pizza3 = new Pizza();
        }

	public boolean setNumberPizzas(int numPizzas) {
                if (!(numPizzas >= 1 && numPizzas <= 3))
                        return false;

                this.numPizzas = numPizzas;
                return true;
	}

	public void setPizza1(Pizza pizza1) {
                this.pizza1.setSize(pizza1.getSize());
                this.pizza1.setNumberOfCheese(pizza1.getNumberOfCheese());
                this.pizza1.setNumberOfPepperoni(pizza1.getNumberOfPepperoni());
                this.pizza1.setNumberOfHam(pizza1.getNumberOfHam());
	}

	public void setPizza2(Pizza pizza2) {
                this.pizza2.setSize(pizza2.getSize());
                this.pizza2.setNumberOfCheese(pizza2.getNumberOfCheese());
                this.pizza2.setNumberOfPepperoni(pizza2.getNumberOfPepperoni());
                this.pizza2.setNumberOfHam(pizza2.getNumberOfHam());
	}

	public void setPizza3(Pizza pizza3) {
                this.pizza3.setSize(pizza3.getSize());
                this.pizza3.setNumberOfCheese(pizza3.getNumberOfCheese());
                this.pizza3.setNumberOfPepperoni(pizza3.getNumberOfPepperoni());
                this.pizza3.setNumberOfHam(pizza3.getNumberOfHam());
	}

	public double calcTotal() {
                int i;
                double cost;
                Pizza[] pizza_arr = { pizza1, pizza2, pizza3 };

                cost = 0.0;
                for (i = 0; i < numPizzas; ++i)
                        cost += pizza_arr[i].calcCost();
                return cost;
	}

}