//package hw9;

public class UnknownCmdException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UnknownCmdException(String message) {
            super(message);
    }
}
