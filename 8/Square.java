//package hw8;

public class Square extends Shape {
	public Square(double length) {
		super(length);
	}

	@Override
	public void setLength(double length) {
		this.length = length;
	}

	@Override
	public double getArea() {
                return (length * length);
	}

	@Override
	public double getPerimeter() {
                return (length * 4);
	}


}
