//package hw8;

import java.math.BigDecimal;

public class Triangle extends Shape {
	public Triangle(double length) {
		super(length);
	}

        @Override
	public void setLength(double length){
		this.length = length;
	}

	@Override
	public double getArea() {                		
		double a=Math.sqrt(3)/4 * length * length;
		double b = new BigDecimal(a)
                .setScale(2, BigDecimal.ROUND_HALF_UP)
                .doubleValue();
		return (b);
		
		
	}

	@Override
	public double getPerimeter() {
                return (length * 3);
	}

}
