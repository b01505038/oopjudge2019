//package hw8;

import java.math.BigDecimal;

public class Circle extends Shape {
	public Circle(double length) {
		super(length);
	}

	@Override
	public void setLength(double length) {
                this.length = length;
	}

	@Override
	public double getArea() {
		double a= length/2 * length/2 * Math.PI;     
		double b = new BigDecimal(a)
                .setScale(2, BigDecimal.ROUND_HALF_UP)
                .doubleValue();
		return (b);
	}

	@Override
	public double getPerimeter() {
               double a=length * Math.PI;
               double b = new BigDecimal(a)
                       .setScale(2, BigDecimal.ROUND_HALF_UP)
                       .doubleValue();
       		return (b);
	}

}
