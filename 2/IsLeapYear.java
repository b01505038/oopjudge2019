

public class IsLeapYear{
	 
    
 public static void main(String[] args) {

                IsLeapYear ily = new IsLeapYear();

                System.out.println(ily.determine(2014));
                System.out.println(ily.determine(2004));
                System.out.println(ily.determine(2100));
       
 }
        

public boolean determine(int year) {
	boolean a = (year % 400 == 0) ||((year % 4 == 0) && (year % 100 != 0));        
    return a;
 
 }
}

