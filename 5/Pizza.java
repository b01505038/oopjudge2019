
public class Pizza {
	public static void main(String[] args) {
			
		Pizza pizza = new Pizza("large", 3, 1, 5);
		System.out.println(pizza.getSize());
		System.out.println(pizza.getNumberOfCheese());
		System.out.println(pizza.getNumberOfPepperoni());
		System.out.println(pizza.getNumberOfHam());
		pizza = new Pizza();
		pizza.setSize("medium");
		pizza.setNumberOfCheese(2);
		pizza.setNumberOfPepperoni(4);
		pizza.setNumberOfHam(1);
		System.out.println(pizza.getSize());
		System.out.println(pizza.getNumberOfCheese());
		System.out.println(pizza.getNumberOfPepperoni());
		System.out.println(pizza.getNumberOfHam());
		System.out.println(pizza.calcCost());
		System.out.println(pizza.toString());
		System.out.println(pizza.equals(new Pizza("large", 2, 4, 1)));
		System.out.println(pizza.equals(new Pizza()));
		System.out.println(pizza.equals(new Pizza("medium", 2, 4, 1)));
	}
	private String size;
	private int numOfCheese;
	private int numOfPepperoni;
	private int numOfHam;

	public Pizza() {
                this("small", 1, 1, 1);
	}

	public Pizza(String size,
                     int numOfCheese,
                     int numOfPepperoni,
                     int numOfHam) {
		this.size = size;
        this.numOfCheese = numOfCheese;
        this.numOfPepperoni = numOfPepperoni;
        this.numOfHam = numOfHam;
	}

	public String getSize() {
       return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public int getNumberOfCheese() {
        return numOfCheese;
	}

	public void setNumberOfCheese(int numOfCheese) {
		this.numOfCheese = numOfCheese;
	}

	public int getNumberOfPepperoni() {
        return numOfPepperoni;
	}

	public void setNumberOfPepperoni(int numOfPepperoni) {
		this.numOfPepperoni = numOfPepperoni;
	}

	public int getNumberOfHam() {
        return numOfHam;
	}

	public void setNumberOfHam(int numOfHam) {
		this.numOfHam = numOfHam;
	}

	public double calcCost() {
                double cost;

                cost = 2 * (numOfCheese + numOfPepperoni + numOfHam);
                if (size.equals("small"))
                        cost += 10;
                else if (size.equals("medium"))
                        cost += 12;
                else if (size.equals("large"))
                        cost += 14;
                else
                        ;

                return cost;
	}

	public boolean equals(Pizza pizza) {
                return (size.equals(pizza.size) &&
                        numOfCheese == pizza.numOfCheese &&
                        numOfPepperoni == pizza.numOfPepperoni &&
                        numOfHam == pizza.numOfHam);
	}

	@Override
	public String toString() {
                return ("size = " + size +
                        ", numOfCheese = " + numOfCheese +
                        ", numOfPepperoni = " + numOfPepperoni +
                        ", numOfHam = " + numOfHam);
	}
}
